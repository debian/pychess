��    4      �  G   \      x     y     �  #   �     �     �     �     �     �          %     9     ?     P  $   \     �     �     �     �     �     �     �  	   �     �     �          	               *     0     5     D     V     d     {     �  	   �  	   �     �     �     �     �     �  
   �     �  	   �  
   �     �  
               _  /     �     �  $   �     �  	   �     �      	     	     )	     F	     X	     ^	     m	  %   }	     �	     �	     �	     �	     �	     �	  	   �	     	
     
     
     1
     8
     @
  
   O
     Z
     b
     f
     w
     �
      �
     �
     �
     �
     �
     �
     �
     �
            
        #  
   *  
   5     @     M     Y     e     $                        /                1      &                         !         #                 ,   *   3          (      -   4         	              .          
   2   '      )   +   %            0         "                                     <b>Animation</b> <b>Enter Game Notation</b> <b>Name of _first human player:</b> <b>Players</b> About Chess Bishop Event: From _Custom Position From _Default Start Position From _Game Notation Gain: Game information How to Play Import games from theweekinchess.com Knight Load _Recent Game Log on as _Guest Minutes: Play _Internet Chess Player _Rating Preferences Promotion Queen Report an issue Rook Round: Save Game _As Share _Game Site: Time Tip of the Day Translate PyChess Use _analyzer Use _inverted analyzer _Accept _Actions _Copy FEN _Copy PGN _Edit _Engines _Export Position _Game _Help _Load Game _Name: _New Game _Password: _Rotate Board _Save Game _Start Game _Use sounds in PyChess Project-Id-Version: pychess
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-13 10:19+0000
Last-Translator: Transifex Bot <>
Language-Team: Afrikaans (http://www.transifex.com/gbtami/pychess/language/af/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: af
Plural-Forms: nplurals=2; plural=(n != 1);
 <b>Animasie</b> <b>Verskaf Spel Notasie</b> <b>Naam van _eerste mens speler:</b> <b>Spelers:</b> Oor skaak Loper Gebeurtenis: Uit _Selfopgestelde Posisie Uit _Standaard Begin Posisie Uit _Spel Notasie Wins: Spel inligting Hoe om te Speel Verkry `n spel uit theweekinchess.com Ridder Laai _Onlangse Spel Teken in as _Gas Minute: Speel _Internet Skaak Speler _Gradering Voorkeure Bevordering Dame Rapporteer `n Probleem Toring Rondte: Stoor Spel _As _Deel Spel Tuiste: Tyd Wenk van die Dag Vertaal PyChess gebruik _analiseerder Gebruik _omgekeerde analiseerder _Aanvaar _Aksies _Kopieër FEN _Kopieër PGN _Wysig _Enjins _Verskeep Posisie _Spel _Hulp _Laai Spel _Naam: _Nuwe Spel _Wagwoord: _Wentel bord _Stoor Spel _Begin Spel _Gebruik klanke in PyChess 