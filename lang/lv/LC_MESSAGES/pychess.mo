��    #      4  /   L           	           7     Q     d  
   s      ~     �     �     �     �     �     �     �     �  )   �       C        ]     k     x          �     �     �     �     �     �     �     �  
   �  
   �     �     �  �  �     }     �     �     �     �  
   �     �               +  
   8     C     L     S     Z  (   c     �  >   �     �     �     �               )     @     O  
   V  
   a     l     t     �     �     �     �                                          !                                         #            "             	                                 
                %(black)s won the game %(white)s won the game <b>_Connect to server</b> <b>_Start Game</b> Analysis by %s Annotation Because %(loser)s was checkmated Black Calculating... Chat Comments Engines Guest Hints Human Being In this position,
there is no legal move. Move History No chess engines (computer players) are participating in this game. Offer Rematch Play Rematch Random Score Threat analysis by %s Undo two moves Welcome White _Actions _Edit _Game _Help _Opponent: _Password: _View _Your Color: Project-Id-Version: pychess
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-13 10:19+0000
Last-Translator: Transifex Bot <>
Language-Team: Latvian (http://www.transifex.com/gbtami/pychess/language/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 %(black)s uzvarēja spēli, %(white)s uzvarēja spēli, <b>_Pievienoties serverim</b> <b>_Sākt spēli</b> %s analīze Anotācija jo "%(loser)s" ir mats Melnie Aprēķina... Tērzēšana Komentāri Dzinēji Viesis Padomi Cilvēks Šajā pozīcijā,
nav legāla gājiena. Gājienu vēsture Šajā spēlē nepiedalās šaha dzinēji (datorspēlētāji). Piedāvāt revanšu Revanšēties Nejaušs Punkti %s apdraudējuma analīze Atcelt divus gājienus Laipni lūdzam Baltie _Darbības R_ediģēt _Spēle _Palīdzība _Pretinieks: _Parole: _Skats _Jūsu krāsa: 