��    9      �  O   �      �     �     �       7   &  *   ^     �     �     �     �     �     �     �  
   �     �     �                     !     /     5     A     H     U     f     o  	   v     �  #   �     �     �  	   �     �     �     �     �                -     5  !   ;     ]     f     l  
   r  	   }     �     �  
   �     �     �     �  
   �     �  "   �       u       �	     �	     �	  2   �	  -   
     1
     :
     ?
     F
     N
     U
     o
     |
     �
     �
     �
     �
     �
     �
     �
                    (     B  	   Q  	   [  
   e  !   p  	   �     �  
   �     �     �     �     �  #   �          )     2  !   :     \     h     p     x     �     �     �     �     �     �     �     �  	   �  &   �          $              (       &                           -                  %          "   #       '   1   
              ,   +      2          )            7             4       8       !      6   3      .             5      	      9       0                 *                /              %s returns an error <b>Play Sound When...</b> <b>Players</b> <big><b>PyChess was not able to save the game</b></big> <big><b>Unable to save file '%s'</b></big> About Chess Beep Bishop Black Clock Close _without Saving Comments Connecting Connection Error Could not save the file Email File exists Game is _lost: Game is _won: Guest Human Being Knight Log on Error Log on as _Guest No sound Normal Open Game Preferences PyChess - Connect to Internet Chess Queen Rook Save Game Save Game _As Score Select sound file... The error was: %s The game ended in a draw Unable to accept %s Unknown White Your opponent is not out of time. _Actions _Game _Help _Load Game _New Game _Replace _Rotate Board _Save Game _Start Game _Use sounds in PyChess _View defends %s draws http://en.wikipedia.org/wiki/Chess improves king safety Project-Id-Version: pychess
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-13 10:19+0000
Last-Translator: Transifex Bot <>
Language-Team: Icelandic (http://www.transifex.com/gbtami/pychess/language/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 %s skilar villu <b>Spila hljóð Þegar...</b> <b>Spilarar</b> <big><b>PyChess gat ekki vistað leikinn</b></big> <big><b>Get ekki vistað skrá '%s'</b></big> Um Skák Bíp Biskup Svartur Klukka _Loka án þess að vista Athugasemdir Tengist Tengingarvilla Gat ekki vistað skrá Tölvupóstur Skráin er þegar til Leikurinn er _tapaður: Leikurinn er _unninn: Gestur Mannvera Riddari Innskráningarvilla Skrá mig inn sem _Gestur Ekkert hljóð Venjulegt Opna Leik Stillingar PyChess - Tengjast Internet Skák Drottning Hrókur Vista leik Vista Leik _Sem Stig Velja hljóðskrá... Villan var: %s Leikurinn hefur endað í jafntefli Get ekki tekið til baka %s Óþekkt Hvítur Andstæðingur rann út á tíma. _Aðgerðir _Leikur _Hjálp _Hlaða inn leik _Nýr leikur _Skipta út Snúa Borði _Vista Leik _Ræsa Leik _Nota hljóð í PyChess _Sýn ver %s jafntefli http://is.wikipedia.org/wiki/Sk%C3%A1k bætir öryggi kóngsins 