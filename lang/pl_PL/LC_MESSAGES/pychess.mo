��            )   �      �     �     �  %   �  $   �                    "     /     7     H     M     T     ]     i     ~     �     �     �     �     �  	   �     �     �     �  
   �  	          
      �  +     )     9  '   E  &   m     �     �     �     �     �     �     �     �     �     �     
     !     '     .     5     G     ^  
   v     �     �     �     �  	   �     �     �               
         	                                                                                                                 Add comment Analyze game Because both players agreed to a draw Because both players ran out of time Bishop Black: Calculating... Edit comment Engines Game information King Makruk No sound Offer _Draw Play _Internet Chess Preferences Rook Round: Save Game _As Select background image file The game ended in a draw Uninstall White: _Fullscreen _Help _Load Game _New Game _Rotate Board _Save Game Project-Id-Version: pychess
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-13 10:19+0000
Last-Translator: Transifex Bot <>
Language-Team: Polish (Poland) (http://www.transifex.com/gbtami/pychess/language/pl_PL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl_PL
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Dodaj komentarz Analiza gry Ponieważ gracze zgodzili się na remis Ponieważ graczom skończył się czas Goniec Czarne: Obliczanie... Edytuj komentarz Silniki Informacje o grze Król Makruk Bez dźwięków Zaproponuj Remis Zagraj przez _Internet Opcje Wieża Runda: Zapisz grę _jako Wybierz obraz dla tła Gra zakończona remisem Odinstaluj Białe: _Pełny ekran _Pomoc _Wczytaj grę _Nowa gra _Obróć planszę _Zapisz grę 