��    [      �     �      �     �     �     �     	     $     >  &   M  *   t     �     �     �     �     �     �     �     �     	  
   	     	     0	     F	  
   L	     W	     ^	     d	     u	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     
     
  	   (
     2
     7
     F
  	   R
  #   \
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
  5   �
          4     N     j     �     �     �     �     �     �     �      �     �       
   
            %   !  
   G     R     ^  	   e     o  
   �     �  
   �     �     �     �  "   �  +   �       ]  ,  E   �  ?   �  %     X   6  '   �  "   �  C   �  i     ?   �  E   �  K     %   Z  #   �     �     �  %   �  	   �  /   �  %   $  6   J     �  &   �     �  
   �  (   �  2      5   3  C   i  5   �     �     �        $     :   ;     v  	   �     �     �  @   �  %   �     "     <  +   S          �  S   �               *     >     N     [  ?   o  /   �  8   �       )   &  w   P  =   �  9     5   @  /   v  	   �  	   �     �     �  V   �  r   ?  J   �  k   �     i  #   �  !   �     �     �  �   �  $   �  $   �     �     �  =   �      <  $   ]  %   �  +   �  U   �     *  )   H  +   r     �         (       V         P          9      <   Q           .   F   ;   O          8           :       1       -         7   	       E   ,           X   '   G         @          #          "                     A   Y       [      +       C   W   &      T   Z   5                     >   *   4   H   R   /   !             I               
   %   )   6   J      ?          2            N   B       M   S   0   =                  $   L           U   3   K              D              '%s' is not a registered name <b>Analyzing</b> <b>Animation</b> <b>Enter Game Notation</b> <b>Play Sound When...</b> <b>Players</b> <big><b>Engine, %s, has died</b></big> <big><b>Unable to save file '%s'</b></big> A player _checks: A player _moves: A player c_aptures: About Chess All Chess Files Bishop Blitz Chess Position Clock Connecting Connection Error Connection was closed Email Enter Game Event: Gain: Game information Game is _drawn: Game is _lost: Game is _set-up: Game is _won: Human Being Knight Lightning Log on Error Log on as _Guest Minutes: Name New Game Normal Observed _ends: Offer _Draw Open Game Ping Player _Rating Preferences Promotion PyChess - Connect to Internet Chess PyChess.py: Queen Rated Rating Rook Round: Save Game _As Send seek Simple Chess Position Site: Spent The connection was broken - got "end of file" message The game ended in a draw The game has been aborted The game has been adjourned The game has been killed Time Type Unknown Unrated Use _analyzer Use _inverted analyzer You sent a draw offer Your opponent asks you to hurry! _Accept _Actions _Call Flag _Game _Help _Hide tabs when only one game is open _Load Game _Log Viewer _Name: _New Game _Observed moves: _Password: _Rotate Board _Save Game _Start Game _Use sounds in PyChess _View http://en.wikipedia.org/wiki/Chess http://en.wikipedia.org/wiki/Rules_of_chess online in total Project-Id-Version: pychess
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-13 10:19+0000
Last-Translator: Transifex Bot <>
Language-Team: Bengali (http://www.transifex.com/gbtami/pychess/language/bn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bn
Plural-Forms: nplurals=2; plural=(n != 1);
 '%s' কোন রেজিস্টার করা নাম নয় <b>অ্যানালাইজ করা হচ্ছে</b> <b>অ্যানিমেশন</b> <b>খেলার সংকেতপদ্ধতি নির্দেশ করো</b> <b>শব্দ কর যখন...</b> <b>খেলোয়ারগন</b> <big><b>ইঞ্জিন, %s, মারা গেছে</b></big> <big><b>'%s' ফাইল সংরক্ষণ করা সম্ভব হচ্ছেনা</b></big> একজন খেলোয়ার চেক করছে: (_c) একজন খেলোয়ার দান দিচ্ছে: (_m) একজন খেলোয়ার ঘুঁটি খেয়েছে: (_a) দাবা সম্বন্ধে দাবার সব ফাইল গজ ব্লিটজ্‌ দাবার অবস্থান ঘড়ি সংযুক্ত করা হচ্ছে সংযোগে ত্রুটি সংযোগ বন্ধ করা হয়েছে ইমেইল খেলা শুরু করুণ ইভেন্ট: লাভ: খেলার তথ্যাবলী খেলাটি ড্র হয়েছে: (_d) খেলাটি হারা হয়েছে: (_l) খেলাটি সেট-আপ করা হয়েছে: (_S) খেলাটি জেতা হয়েছে: (_w) মানুষ ঘোড়া লাইটনিং লগ-অন-এ ত্রুটি অতিথি হিসাবে লগ-অন করো মিনিট: নাম নতুন খেলা নরমাল পর্যবেক্ষিত অন্তসমূহ: (_O) ড্র অফার করো (_D) খেলা খোলো পিং করুন খেলোয়ারের রেটিং পছন্দসমূহ পদোন্নতি পাইচেস - ইন্টারনেটে সংযুক্ত করো PyChess.py: মন্ত্রী রেট করা রেটিং নৌকা রাউন্ড: অন্য নামে খেলা সেভ করো (_A) সন্ধান প্রেরন করো সাধারণ দাবার অবস্থান সাইট: ব্যায় করা হয়েছে যোগাযোগ বিচ্ছিন্ন হল — "end of file" মেসেজ পাওয়া গেছে খেলাটি ড্র-এ অন্ত হয়েছে খেলাটি বন্ধ করা হয়েছে খেলাটি স্থগিত হয়েছে খেলাটি মারা হয়েছে সময় ধরণ অজ্ঞাত রেট না করা অ্যানালাইজার ব্যাবহার করা হোক (_a) ইনভার্টেড অ্যানালাইজার ব্যাবহার করা হোক (_a) তুমি একটি ড্র-এর অফার পাঠালে আপনার প্রতিদ্বন্দি আপনাকে তারা দিচ্ছেন! গ্রহণ করো (_A) ক্রিয়াসমূহ (_K) কল ফ্ল্যাগ (_C) খেলা (_G) সহায়িকা (_H) একটি মাত্র খেলা খোলা থাকলে ট্যাবগুলি লুকোনো থাকুক (_H) খেলা লোড করো(_L) লগ প্রদর্শক (_L) নাম:(_N) নতুন খেলা (_N) পর্যবেক্ষিত দানসমূহ: (_O) পাসওয়ার্ড:(_P) বোর্ড ঘোরাও (_R) খেলা সেভ করো (_S) খেলা আরম্ভ করো (_S) পাইচেস-এ শব্দ ব্যাবহার করা হোক (_U) প্রদর্শন (_V) http://bn.wikipedia.org/wiki/দাবা http://en.wikipedia.org/wiki/Rules_of_chess অনলাইন মোট 