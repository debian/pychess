��    t      �  �   \      �	     �	     �	  #   �	  $   
     3
     D
     S
     f
     r
  *   y
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     	                    $  	   +     5     8     ;     C     F     W     Y     e     h  $   y     �     �     �     �     �     �     �     �     �  C   �     4     G     T     `     m     {     �     �     �     �     �  	   �     �     �     �     �     �     �     �     �     
          !     )     7     F     L     N     Q  9   T  3   �     �     �     �     �  	   �     �     �     �  	                       (  	   6  	   @     J     P     Y     j     p     y  
        �  	   �     �  
   �     �  
   �     �     �     �     �     �            j       y     {     �  !   �     �  
   �  
   �     �     �  $        )  
   +     6     C     J     L     O     R     a     n  	   v     �     �     �  
   �     �     �  	   �  	   �     �     �     �     �     �     �     �     �     �  (        B     U     W     _     v     �     �     �     �  H   �     �                3     H     [     m     o     t     |     �  
   �     �     �     �     �     �     �     �     �       
        %     ,     =     R     X     Z     ]  8   `  9   �     �     �     �     �     �                 	             )     /     8     H     V     d     m     v     �     �     �     �     �     �  	   �  	   �     �     �     �  
   �               $     1     8        4       	   :   A   m      6          M   D          V   )                   b   h   j      Z          U   1                  O   =   E      c   e   G      2           g                 l   a           i   d   k   f           S   !                        /   #      $       C   *   W      .   H   (   >            9          3             ,   L       @       X          B   ?   F   <       -           r       Q       8   P   "   '   
   ^   %   [   +   7   T          \   n               _   ]   o   q   `   K         p       ;              s   I      0   t   Y           &   J   5   R   N    * <b>General Options</b> <b>Name of _first human player:</b> <b>Name of s_econd human player:</b> <b>Open Game</b> <b>Players</b> <b>_Start Game</b> About Chess Accept Auto _rotate board to current human player B Black O-O Black O-O-O Black: C CA CM Calculating... Chess client Clear Comments Computer Computers D Database Decline Engines Event: Externals FA FM Friends GM Game information H How to Play IM Import chessfile Import games from theweekinchess.com Initial position K King Load _Recent Game Move number N NM New Game New _Database No chess engines (computer players) are participating in this game. Offer Ad_journment Offer _Abort Offer _Draw Offer _Pause Offer _Resume Offer _Undo P Pause Play Play _Internet Chess Player _Rating Png image Preferences PyChess Information Window Q R Round: SR Save Save Game _As Save database as Score Search: Select engine Setup Position Site: T TD TM The displayed name of the first human player, e.g., John. The displayed name of the guest player, e.g., Mary. Themes Tip of the Day Translate PyChess U Uninstall WFM WGM WIM White O-O White O-O-O White: _Actions _Analyze Game _Copy FEN _Copy PGN _Edit _Engines _Export Position _Game _General _Help _Load Game _Name: _New Game _Next _Password: _Player List _Save Game _Sounds _Start Game _Use sounds in PyChess _View _Your Color: black white Project-Id-Version: pychess
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-13 10:19+0000
Last-Translator: Transifex Bot <>
Language-Team: Basque (Spain) (http://www.transifex.com/gbtami/pychess/language/eu_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 * Aukera orokorrak Lehen giza jokalariaren izena: Bigarren giza jokalariaren izena: Ireki Jokoa Jokalariak Hasi Jokoa Xakeari Buruz Onartu Biratu taula egungo giza jokalariari B Beltza O-O Beltza O-O-O Beltza C CA CM Kalkulatzen... Xake bezeroa Garbitu Iruzkinak Ordenagailua Ordenagailuak D Datu-basea Ukatu Motoreak Ekitaldia Kanpokoak FA FM Lagunak GM Joko informazioa H Nola Jolastu IM Xake-fitxategia Inportatu Jokoak inportatu theweekinchess.com-etik Hasierako posizioa K Erregea Arestiko Jokoa Kargatu Mugitu Zenbakia N NM Joko berria Datu-Base berria Ez dago xake-motorrik (ordenagailu jokalaria) joko honetan parte hartzen Atzerapena Eskaini Bertan Behera Uztea Eskaini Berdinketa Eskaini Atsedenaldia Eskaini Jarraitzea Eskaini Zuzentzea Eskaini P Eten Jolastu _Internet Xake Partida Jokalarien Balorazioa Png Irudia Lehentasunak PyChess Informazio Leihoa Q R Txanda SR Gorde Gorde Jokoa Honela Gorde datu-basea honela Puntuazioa Bilatu Aukeratu motorea Posizioa Konfiguratu Gunea T TD TM Erakutsitako lehen giza jokalaria, adibidez, John izena. Jokalari-gonbidatuaren bistaratutako izena, adib., Maite. Gaiak Eguneko aholkua Itzuli PyChess U Desinstalatu WFM WGM WIM Zuria O-O Zuria O-O-O Zuria Ekintzak Jokoa Analizatu FEN-a Kopiatu PGN-a Kopiatu _Editatu Motoreak Esportatu Posizioa Jokoa Orokor Laguntza Jokoa Kargatu Izena Joko berria Hurrengoa Pasahitza Jokalari Zerrenda Gorde Jokoa Soinuak Hasi Jokoa PyChess-en soinua erabili Ikusi Zure Kolorea beltza zuria 